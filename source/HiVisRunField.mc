using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Graphics;
using Toybox.System as System;
using Toybox.UserProfile as UserProfile;
using Toybox.Time as Time;
using Toybox.AntPlus;
using Toybox.Position;
using Toybox.Lang;
using Toybox.Math;
using Toybox.Time;
using Toybox.Attention;





//! @author Brian Bannister, first version of this started from BikersFields by Konrad Paumann

class HiVisRunField2 extends App.AppBase {
    hidden var view;

    function initialize() {
        App.AppBase.initialize();
    }

    function getInitialView() {
        view = new HiVisRunView();
        onSettingsChanged();
        return [ view ];
    }


    function onSettingsChanged() {

    }

    function onStop(state) {
        view.onStop(state);
    }

}
//Add Dynamic Class
class RunningDynamicsListen extends AntPlus.RunningDynamicsListener {
    hidden var updateRunningDynamicValue = null;
    function initialize() {
        RunningDynamicsListener.initialize();
    }
    function onRunningDynamicsUpdate(data){
       updateRunningDynamicValue = data;
    }
}

//! A DataField deisgned to fill the entire screen
//! @author Brian Bannister
class HiVisRunView extends Ui.DataField {

    hidden var SCREEN_SIZE = 260;
    hidden var LARGE_FONT = Graphics.FONT_SYSTEM_NUMBER_THAI_HOT;
    hidden var MEDIUM_FONT = Graphics.FONT_SYSTEM_NUMBER_MEDIUM;
    hidden var LABEL_FONT = Graphics.FONT_SYSTEM_XTINY;
    
    hidden var BIG_VERTICAL_DOWNSET = 0;
    hidden var BIG_INSET = 0;
    hidden var SMALL_INSET = 0;
    hidden var SMALL_VERTICAL_DOWNSET = 0;
    hidden var SMALL_SECOND_VERTICAL_DOWNSET = 0;
    hidden var BIG_SECOND_VERTICAL_DOWNSET = 0;
    hidden var BIG_LABEL_DOWNSET = 0;
    
    hidden var SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    hidden var BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    hidden var SMALL_BOUNDING_BOX_RELATIVE_DROP = 0;
    hidden var BIG_BOUNDING_BOX_RELATIVE_DROP = 0;
    
    hidden var LABEL_FONT_SPACING = 0;
    hidden var SMALL_LABEL_FONT_DOWNSET = 0;
    
    hidden var SHORT_LABEL_NUM_LETTERS = 3;
    hidden var LONG_LABEL_NUM_LETTERS = 4;
    
    hidden var SUFFER_TEXT_DOWNSET = 4;
    hidden var SUFFER_BOX_HEIGHT = 23;

	hidden var scrollProgress = 0;

    hidden var kmOrMileInMeters = 1000;
    hidden var textColor = Graphics.COLOR_BLACK;
    hidden var backgroundColor = Graphics.COLOR_WHITE;
    hidden var headerColor = Graphics.COLOR_DK_GRAY;

    hidden var hrZone0 = 114;
    hidden var hrZone1 = 126;
    hidden var hrZone2 = 138;
    hidden var hrZone3 = 150;
    hidden var hrZone4 = 165;
    hidden var hrZone5 = 175;

    hidden var currentHR = 0;
    hidden var currentZone = 0;
    hidden var averageHR = 0;

    hidden var mafHR = 0;
    hidden var hasMaf = false;
    hidden var mafLowerBound = 0;
    hidden var mafMiddleBound = 0;
    hidden var mafHigherBound = 0;
    
    hidden var timeInMaf = 0;
    hidden var timeOverMaf = 0;

    hidden var currentPace = 0;
    hidden var averagePace = 0;

    hidden var distance = 0;
    hidden var elapsedTime = 0;
    hidden var trainingEffect = 0;

    hidden var ascent = 0.0;
    hidden var currentCadence = 0;
    hidden var currentSpeed = 0;

    //Adding Dynamic Pace
    hidden var hrmpro = null;
    hidden var runningDynamicFunction;
    hidden var runningDynamicValue = null;
    hidden var dynamicCadence = 0;
    hidden var dynamicStepLenght = 0;
    hidden var dynamicSpeed = 0;
    hidden var dynamicPace = 0;
    hidden var cadSpeed = 0;
    hidden var cadPace = 0;
	//Adding Running Economy 100m variable///////////////////////////////
	hidden var uRestingHeartRate 	= 0;
	
	hidden var mTimerRunning = false;

	hidden var mPrevElapsedDistance = 0;

	hidden var mLaps 					 = 1;
	hidden var mLastLapDistMarker 		 = 0;
    hidden var mLastLapTimeMarker 		 = 0;
    hidden var mLapHeartRateAccumulator  = 0;

	hidden var mRecentHR = new [30];
	hidden var curPosRE;
	
	hidden var mLastLapTimerTime 		= 0;
	hidden var mLastLapElapsedDistance 	= 0;
	hidden var mLastLapEconomy			= 0;

	hidden var mLastNDistanceMarker = 0;
	hidden var mLastNEconomySmooth	= 0;
	hidden var mAverageEconomy		= 0;
	hidden var mLapEconomy			= 0;
	
	hidden var mTicker 		= 0;
	hidden var mLapTicker	= 0;
	
	hidden var mEconomyField 		= null;
	hidden var mAverageEconomyField = null;
	hidden var mLapEconomyField 	= null;
	hidden var runningEconomy					= 0;
	//End of Running Economy 100m variable////////////////////////////////

    //Adding Accurate Pace base on GPS Location/////////////////////////////
    hidden var accuratePace = 0;
    hidden var updateAccuratePace = 0;
    hidden var smoothenedPace = 0;
    hidden var currenAccuratetPace  = 0;
    hidden var weight = 0;
    hidden var elapsedAccurateTime = 0;
    hidden var elapsedAccurateDistance = 0;
    hidden var position = 0;
    // @see https://en.wikipedia.org/wiki/Earth_ellipsoid#Historical_Earth_ellipsoids
	protected var earthMeanRadius = 6378137.0;
	protected var units;

	// will hold the most recent GPS positions & their timestamps
	protected var recentLocations = [];
	protected var recentProjections = [];
	protected var recentMoments = [];
	protected var relevancyWindow = 20; // in seconds
	protected var lastLocation = new Position.Location({:latitude => 0, :longitude => 0, :format => :radians});
	protected var lastMoment = new Time.Moment(0);

	// minimum required amount of recent coordinates (within window) to start projecting from
	protected var reliabilityMinimum = 3;
    ///////////////////////////////////////End of Accurate Pace/////////////////////////////////////////////

	//////////////Adding HR Snapshot///////////////////////////////////
	
	var alertHR;
    var heartRate;
	var heartRateLabel;
	var heartRateZones;
	var avgHR;
	var hrColor;
	hidden var hrBackgroundColor = Graphics.COLOR_WHITE;
	
	var firstField;
	var firstFieldMode;
	var secondField;
	var secondFieldMode;
	var bottomField;
	var bottomFieldMode;

	var invertMiddleBackground;
	var foregroundColour;
	var backgroundColour;
	var lineColour;

	var arrayColours = new [5];
    var arrayHRValue = new [189];
    var arrayHRZone = new [189];
	var curPos;
	var aveHRValue;
	var aveHRCount;
	var HRmin;
	var HRmax;
	var HRmid;
	var HRLayout = false;
	hidden var zone1Max;
	hidden var zone2Max;
	hidden var zone3Max;
	hidden var zone4Max;

    ///////////////End of HR Snapshot//////////////////////////////////////


	//////////////Adding Virtual Pace Variable/////////////////////////////////
	hidden var sysCurrentSpeed=0;
    hidden var sysElapsedDistance = 0;
    hidden var sysElapsedTime=0;
	hidden var sysRunDistance = 0;
    hidden var vRunDistance=0;
    hidden var vTimeDiff = 0;
    hidden var vDistanceDiff=0;
    hidden var sysRunTime=0;
    hidden var vRunTime=0;
    hidden var vDiffColor=Graphics.COLOR_WHITE;
    hidden var sysTimerTime=0;

	hidden var virtualPace=0;
    hidden var virtualSpeed = 0;
    hidden var vPaceMinute=0;
    hidden var vPaceSecond = 0;

	//////////////Ending Virtual Pace//////////////////////////////////


	/////////////////////////Adding Lap Pace Variable///////////////////
	hidden var lastLapStartDistance = 0;
	hidden var showLapMetrics = false;
	hidden var referenceDistance = 1000;
	hidden var lapAvgPace = 0;
	hidden var lastLapStartTimer = 0;
	hidden var paceChartData = new DataQueueHR(5);
	/////////////////////////Ending Lap Pace Variable///////////////////



    //Suffer Score Values
    //Activities with a Suffer Score of 100 to 150 will be classified as "tough;" Green
    //those with a Suffer Score of 151 to 250 are "Extreme;" Yellow
    //and those with a suffer Suffer Score above 250 are "Epic." Red
    hidden var sufferScore = 0.0;
    hidden var lastMeasuredSufferTime = 0;
    
    hidden enum {
    	F245,
    	F245M,
    	F255, 
    	F255M,
    	F255S,
    	F255SM,
    	F6Pro,
        F6XPro,
        F645,
        F645M,
        F6,
        F945,
        F945LTE,
        F5XPlus,
        F6SPro,
        F6S,
        F6C,
        F7,
        F7S,
        F7X,
        F5X,
        F5SPlus,
        F5S,
        F5Plus,
        F5,
        F745,
        F935,
        Venu2,
        Venu2S,
        Venu,
        VenuMB,
        Vivo4,
        Vivo4S
    }
    hidden var watchType = F6Pro;
    


    function initialize() {
        hrmpro = new AntPlus.RunningDynamics (new RunningDynamicsListen());
        DataField.initialize();
        var profile = UserProfile.getProfile();
        var sport = UserProfile.getCurrentSport();
        if( profile != null && sport != null) {
            var heartRateZones = UserProfile.getHeartRateZones(sport);
            if (null != heartRateZones) {
                if (null != heartRateZones[0]) {
                    hrZone0 = heartRateZones[0];
                }
                if (null != heartRateZones[1]) {
                    hrZone1 = heartRateZones[1];
                }
                if (null != heartRateZones[2]) {
                    hrZone2 = heartRateZones[2];
                }
                if (null != heartRateZones[3]) {
                    hrZone3 = heartRateZones[3];
                }
                if (null != heartRateZones[4]) {
                    hrZone4 = heartRateZones[4];
                }
                if (null != heartRateZones[5]) {
                    hrZone5 = heartRateZones[5];
                }
            }
            var year = 1970 + Time.now().value() / Time.Gregorian.SECONDS_PER_YEAR;
            mafHR = 180 - (year  - profile.birthYear);
            var useStrictMaf = getPropertyById("Strict_MAF_prop");
            if (useStrictMaf) {
	            mafLowerBound = mafHR - 11;
	            mafMiddleBound = mafHR - 6;
	            mafHigherBound = mafHR + 2;
            }
            else {
            	mafLowerBound = getPropertyById("Low_MAF_prop");
            	mafHigherBound = getPropertyById("High_MAF_prop");
            	if (mafHR > mafLowerBound && mafHR < mafHigherBound) {
            		mafMiddleBound = mafHR;
            	}
            	else {
            		mafMiddleBound = mafLowerBound + (mafHigherBound - mafLowerBound) / 2;
            	}
            }
        }
        
        SCREEN_SIZE = System.getDeviceSettings().screenWidth;
        setWatchType();
        setWatchLayout(watchType);
        
        //System.println("screenWidth = " + SCREEN_SIZE);
        //System.println("screenHeight = " + System.getDeviceSettings().screenHeight);
        //printWatchType();
        
		//Adding Running Economy to FIT file
		var mProfile 		= UserProfile.getProfile();
 		uRestingHeartRate 	= mProfile.restingHeartRate;
 		
 		mEconomyField 		 = createField("running_economy", 0, FitContributor.DATA_TYPE_FLOAT, { :mesgType=>FitContributor.MESG_TYPE_RECORD, :units=>"cm/bpm" });
        mAverageEconomyField = createField("average_economy", 1, FitContributor.DATA_TYPE_FLOAT, { :mesgType=>FitContributor.MESG_TYPE_SESSION, :units=>"cm/bpm" });
        mLapEconomyField 	 = createField("lap_economy", 	  2, FitContributor.DATA_TYPE_FLOAT, { :mesgType=>FitContributor.MESG_TYPE_LAP, :units=>"cm/bpm" });
       	//mEconomyField           = createField("running_economy", 0, FitContributor.DATA_TYPE_UINT16, { :mesgType=>FitContributor.MESG_TYPE_RECORD,  :units=>"cm/bpm" });
        //mAverageEconomyField    = createField("average_economy", 1, FitContributor.DATA_TYPE_UINT16, { :mesgType=>FitContributor.MESG_TYPE_SESSION, :units=>"cm/bpm" });
        //mLapEconomyField        = createField("lap_economy",     2, FitContributor.DATA_TYPE_UINT16, { :mesgType=>FitContributor.MESG_TYPE_LAP,     :units=>"cm/bpm" });

        for (var i = 0; i < mRecentHR.size(); ++i) {
            mRecentHR[i] = 0.0;
        }
        curPosRE = 0;
		//End of Adding Running Economy to Fit file

		 //Adding Snapshot HR/////////////
		HRLayout= App.getApp().getProperty("HR_Graph");
        var usePreferences = 1;
        var background = 0;  // 0=white; 1=black
		invertMiddleBackground = false;  // ... of the graph only
        firstFieldMode = 2;  // 0=time-of-day, 1=battery, 2=aveHR, 3=CAD, 4=aveCAD ... -1=none
		secondFieldMode = 0;
		bottomFieldMode = 0;
        foregroundColour = Graphics.COLOR_BLACK;
		backgroundColour = Graphics.COLOR_WHITE;
		lineColour = Graphics.COLOR_LT_GRAY;
        
        zone1Max = getPropertyById("zone1Max");
		zone2Max = getPropertyById("zone2Max");
		zone3Max = getPropertyById("zone3Max");
		zone4Max = getPropertyById("zone4Max");
		alertHR = getPropertyById("AlertHR");
		heartRateZones = [90, zone1Max, zone2Max, zone3Max, zone4Max, 195];

		HRmid = ( heartRateZones[0] + (heartRateZones[5]-heartRateZones[0])*0.5 ).toNumber();

        //if (usePreferences == 1) {
		//	background = Application.getApp().getProperty("blackBackground");
		//	invertMiddleBackground = Application.getApp().getProperty("invertMiddleBackground");
		//	firstFieldMode = Application.getApp().getProperty("firstFieldMode");
		//	secondFieldMode = Application.getApp().getProperty("secondFieldMode");
		//	bottomFieldMode = Application.getApp().getProperty("bottomFieldMode");
			
		//}
        //if (background == 1) {
		//	foregroundColour = Gfx.COLOR_WHITE;
	//		backgroundColour = Gfx.COLOR_BLACK;
	//		lineColour = Gfx.COLOR_DK_GRAY;
	//	} else {
			
	//	}

        for (var i = 0; i < arrayHRValue.size(); ++i) {
            arrayHRValue[i] = 0;
            arrayHRZone[i] = -1;
        }

        curPos = 0;
        aveHRValue = 0;
        aveHRCount = 0;
        arrayColours = [Graphics.COLOR_LT_GRAY, Graphics.COLOR_BLUE, Graphics.COLOR_DK_GREEN, Graphics.COLOR_ORANGE, Graphics.COLOR_DK_RED];

        
        //End of Adding SnapshotHR////////////////////////////////
		////////////////////Adding Virtual Pace Initialize///////////////////////////////
        virtualPace = Application.getApp().getProperty("virtualPace");
        var findIndex = 0;
        findIndex = virtualPace.find(":");
        vPaceMinute = (virtualPace.substring(0, findIndex)).toFloat();
        vPaceSecond = (virtualPace.substring(findIndex+1, 5)).toFloat();
        virtualSpeed = (((vPaceMinute * 60)+vPaceSecond));

		//////////////Ending Virutal Pace Initialize///////////////////////////////////////

		
    }


    function onStop(state) {
		
    }

    //! The given info object contains all the current workout
    function compute(info) {
        
        if (info.currentSpeed != null) {
            currentPace = (.6 * currentPace) + (.4 * getPaceFromSpeed(info.currentSpeed));
        }
        if (null != info.averageSpeed) {
            averagePace = getPaceFromSpeed(info.averageSpeed);
        }

        currentHR = resultOrZero(info.currentHeartRate);
        averageHR = resultOrZero(info.averageHeartRate);
        currentCadence = resultOrZero(info.currentCadence);
        currentSpeed = resultOrZero(info.currentSpeed);
		
        // these two must be called before elapsedTime is updated and after HR is updated.
        setTimeInMaf(info.timerTime);
        setTimeOverMaf(info.timerTime);

        elapsedTime = resultOrZero(info.timerTime);
        distance = resultOrZero(info.elapsedDistance);
        trainingEffect = resultOrZero(info.trainingEffect);

        currentZone = getHRZone(currentHR);

        ascent = resultOrZero(info.totalAscent);

        setSufferScore();

        //Adding Dynamic Value to compute   
        runningDynamicValue = hrmpro.getRunningDynamics();
        if (runningDynamicValue != null){
        //        runningDynamicValue.onRunningDynamicsUpdate(data){
                dynamicCadence = runningDynamicValue.cadence * 2;
                //get step lenght and conver to meter from milimeter
                dynamicStepLenght = runningDynamicValue.stepLength*0.001;
                // Speed (m/s) = Cadence (step/s) x step lenght (m)
                
           }
           
                dynamicSpeed = dynamicCadence * dynamicStepLenght;
                cadSpeed = currentCadence * dynamicStepLenght;
                cadPace = getPaceFromXSpeed(cadSpeed);
                dynamicPace = getPaceFromXSpeed(dynamicSpeed);
                ////////////////////////////////////////////////
                //cadSpeed = (currentCadence/60) * (dynamicStepLenght/1000);
                
 ///////////////////////////Adding Accurate Compute Info Code/////////////////////////////
        updateLocation();
        
		var relevantSecondsAgo = 5;
		var recentPaces = [];
		var recentDurations = [];
		for (var i = self.recentMoments.size() - 1; i > 0; i--) {
			if (Time.now().compare(self.recentMoments[i]) > relevantSecondsAgo) {
				break;
			}

			elapsedAccurateTime = self.recentMoments[i].compare(self.recentMoments[i - 1]);
			elapsedAccurateDistance = self.calculateDistance(self.recentProjections[i - 1], self.recentProjections[i]);
			if (elapsedTime > 0) {
				recentDurations.add(elapsedTime);
				updateAccuratePace = elapsedAccurateDistance > 0 ? elapsedAccurateTime / elapsedAccurateDistance : 0;
				recentPaces.add(updateAccuratePace);
			}
		}

		if (recentPaces.size() > 1) {
			// instead of using the average pace over the last few measurements, we're going
			// to average the 40, 50 and 60 weighted percentile:
			// - an average on the whole dataset would make the results slow: it would take
			//   awhile for a slowdown to show up (weighed up by older fast paces), and the
			//   momentary slowness would drag us down for quite awhile longer
			// - on the other hand, using the mean (or 50% quantile) saves us from those
			//   outliers, but will make for a very jumpy pace since it's only 1 value...
			weight = new WeightedQuantile(recentPaces, recentDurations);
			smoothenedPace = (weight.calculate(0.4) + weight.calculate(0.5) + weight.calculate(0.6)) / 3;
			currenAccuratetPace = recentPaces[recentPaces.size() - 1];

			// in order to compare the 2, it'll be more useful to convert them back to
			// a "distance" unit rather than pace (time over distance)
			var smoothenedDistance = smoothenedPace > 0 ? 1 / smoothenedPace : 0;
			var currentDistance = currenAccuratetPace > 0 ? 1 / currenAccuratetPace : 0;
			var diff = currentDistance > smoothenedDistance ? currentDistance - smoothenedDistance : smoothenedDistance - currentDistance;

			if (
				diff > smoothenedDistance / 6 &&
				diff < smoothenedDistance &&
				info has :currentSpeed && info.currentSpeed != null && info.currentSpeed > 0 &&
				diff < 1 / info.currentSpeed / 3
			) {
				// there's a significant enough change in pace (though still realistic - a
				// change *too big* is likely a GPS correction) and it's backed up by sensors
				updateAccuratePace = currenAccuratetPace;
			} else {
				// change in pace is either unrealistic, or insignificant enough that we'll
				// want to keep showing a smooth-ish pace
				updateAccuratePace = smoothenedPace;
			}
		} else if (info has :currentSpeed && info.currentSpeed != null && info.currentSpeed > 0) {
			// try to fall back to speed provided by system (possibly from other sources, like
			// foot pods or accelerometer) if we have no reliable own pace
			updateAccuratePace = 1 / info.currentSpeed;
		}

		if (updateAccuratePace == 0 || updateAccuratePace > 2.5) {
			return "--:--";
		}

		accuratePace = updateAccuratePace / (60.0 / 1000.0);
		//return accuratePace;
	

      //////////////////////////////////////////////////////////////////////////////////////End Accurate Compute Info//////////////////////////////////
      
		///////////Adding Snapshot HR compute info/////////////////////////
        heartRate = resultOrZero(info.currentHeartRate);
		avgHR = resultOrZero(info.averageHeartRate);
				
		if (heartRate != null && heartRate >= heartRateZones[0] && info.elapsedTime != null && info.elapsedTime > 0) {

			aveHRValue = aveHRValue + heartRate;
			aveHRCount = aveHRCount + 1;
			
			if(aveHRCount > 3) {
			
				arrayHRValue[curPos] = (aveHRValue / aveHRCount).toNumber();	
			
				if (arrayHRValue[curPos] >= heartRateZones[0] && arrayHRValue[curPos] < heartRateZones[1]) {
					arrayHRZone[curPos] = 0;
				} else if (arrayHRValue[curPos] >= heartRateZones[1] && arrayHRValue[curPos] < heartRateZones[2]) {
					arrayHRZone[curPos] = 1;
				} else if (arrayHRValue[curPos] >= heartRateZones[2] && arrayHRValue[curPos] < heartRateZones[3]) {
					arrayHRZone[curPos] = 2;
				} else if (arrayHRValue[curPos] >= heartRateZones[3] && arrayHRValue[curPos] < heartRateZones[4]) {
					arrayHRZone[curPos] = 3;
				} else if (arrayHRValue[curPos] >= heartRateZones[4]) {
					arrayHRZone[curPos] = 4;
				}

				HRmin = HRmid + 5;
				HRmax = HRmid - 5;

        		for (var i = 0; i < arrayHRValue.size(); ++i) {
        			if(arrayHRZone[i] >=0) {
        	
        				if(arrayHRValue[i] > HRmax) {
        					HRmax = arrayHRValue[i];
        				} else if(arrayHRValue[i] < HRmin) {
        					HRmin = arrayHRValue[i];
        				}
        		
        			}        		
				}

				HRmin = HRmin - 15;
				if(HRmin < heartRateZones[0] + 10) { HRmin = heartRateZones[0] + 10; }  // set floor just above min HR

				HRmax = HRmax + 15;
				if(HRmax > heartRateZones[5] + 5) { HRmax = heartRateZones[5] + 5; }  // clip spikes just above max HR

//				Sys.println("" + curPos + " " + arrayHRValue[curPos] + " " + arrayHRZone[curPos] + " " + HRmin + " " + HRmax);

				curPos = curPos + 1;
				if(curPos > arrayHRValue.size()-1) {
					curPos = 0;
				}
				
				aveHRCount = 0;
				aveHRValue = 0;
				
			}
			
		}		
		////////////////////////Add Alert HR////////////////
		if (heartRate > alertHR ){
			Attention.playTone(Attention.TONE_DISTANCE_ALERT);
		}
		////////////////////////Ending Alert HR///////////////

		if (heartRate == null || heartRate < heartRateZones[0]) {
			heartRateLabel = "Heart Rate";
		} else if (heartRate >= heartRateZones[0] && heartRate < heartRateZones[1]) {
			heartRateLabel = "WARM UP";
			hrColor = Graphics.COLOR_LT_GRAY;
            hrBackgroundColor = Graphics.COLOR_LT_GRAY;
		} else if (heartRate >= heartRateZones[1] && heartRate < heartRateZones[2]) {
			heartRateLabel = "EASY";
			hrColor = Graphics.COLOR_BLUE;
            hrBackgroundColor = hrColor;
		} else if (heartRate >= heartRateZones[2] && heartRate < heartRateZones[3]) {
			heartRateLabel = "AEROBIC";
			hrColor = Graphics.COLOR_GREEN;
            hrBackgroundColor = hrColor;
		} else if (heartRate >= heartRateZones[3] && heartRate < heartRateZones[4]) {
			heartRateLabel = "THRESHOLD";
			hrColor = Graphics.COLOR_ORANGE;
            hrBackgroundColor = hrColor;
		} else if (heartRate >= heartRateZones[4]) {
			heartRateLabel = "MAXIMUM";
			hrColor = Graphics.COLOR_RED;
            hrBackgroundColor = hrColor;
		}

		heartRate = toStr(heartRate);

		if (firstFieldMode >= 0) {
			if (firstFieldMode == 0) {
				firstField = fmtTime(System.getClockTime());
			} else if (firstFieldMode == 1) {
				firstField = toStr(System.getSystemStats().battery.toNumber()) + "%";
			} else if (firstFieldMode == 2) {
				firstField = toStr(info.averageHeartRate);
			} else if (firstFieldMode == 3) {
				firstField = toStr(info.currentCadence);
			} else {
				firstField = info.averageCadence;
				if (firstField != null) {
					firstField = firstField * 2;
				}
				firstField = toStr(firstField);
			}
		}

		if (secondFieldMode >= 0) {
			if (secondFieldMode == 0) {		
				secondField = fmtTime(System.getClockTime());
			} else if (secondFieldMode == 1) {
				secondField = toStr(System.getSystemStats().battery.toNumber()) + "%";
			} else if (secondFieldMode == 2) {		
				secondField = toStr(info.averageHeartRate);
			} else if (secondFieldMode == 3) {
				secondField = toStr(info.currentCadence);
			} else {
				secondField = info.averageCadence;
				if (secondField != null) {
					secondField = secondField * 2;
				}
				secondField = toStr(secondField);
			}
		}

		if (bottomFieldMode == 0) {
		
			var time;
			time = info.timerTime;
	
			if (time != null) {
				time /= 1000;
			} else {
				time = 0.0;
			}
	
			bottomField = fmtSecs(time);
		} else {
		
			bottomField = toDist(info.elapsedDistance);

		}

        ////////////End of Adding Snapshot HR compute info///////////////////////


		////////////Adding Virtual Pace Compute Info////////////////////////////////////////////////////////////////////
		sysElapsedDistance = (resultOrZero(info.elapsedDistance)/1000).format("%.2f");
        sysElapsedTime = fmtSecs((resultOrZero(info.timerTime)/1000));
        sysTimerTime = info.timerTime/1000;
       
        sysCurrentSpeed = (info.currentSpeed).format("%.2f");
        if (info.currentSpeed != 0){
            sysRunTime = ((resultOrZero(info.elapsedTime))/info.currentSpeed);
        }
        else {
             sysRunTime = 0;   
        }
        
        vRunDistance = (((resultOrZero(info.elapsedDistance)))*(virtualSpeed/1000));
        vTimeDiff = ( vRunDistance-sysTimerTime);
        if (vTimeDiff >=0){
            if (vTimeDiff <=60){
            vTimeDiff = vTimeDiff.format("%.1d");
            vDiffColor = Graphics.COLOR_GREEN;
            }
            else {
            vTimeDiff = minutesToTimePacer(vTimeDiff);
            vDiffColor = Graphics.COLOR_GREEN;
            }

        }
        else{
            if (vTimeDiff <=60){
                vTimeDiff = vTimeDiff.format("%.1d");
                vDiffColor = Graphics.COLOR_RED;
            }
            else{         
                vTimeDiff = minutesToTimePacer(vTimeDiff);
                vDiffColor = Graphics.COLOR_RED;
            }
        }
		///////////Ending Virtual Pace Compute Info///////////////////////////////////////////////////////////////

		////////////Adding Lap Pace Compute Info////////////////////////////////////////
		if (distance != lastLapStartDistance) {
            lapAvgPace = (elapsedTime-lastLapStartTimer) * referenceDistance/1000 / (distance-lastLapStartDistance);
            lapAvgPace = lapAvgPace.toNumber();
            
        } else {
            if(distance>0){
                lapAvgPace = elapsedTime * referenceDistance/1000 / distance;
            }
        }
		////////////Ending Lap Pace Compute Info///////////////////////////////////////	

	//Adding Running Economy Compute info/////////////////////////////////////
	  if (mTimerRunning) {  //! We only do calculations if the timer is running (distance isn't accumulated otherwise)
    		mTicker++;
	        mLapTicker++;

    		var mElapsedDistance 		= (info.elapsedDistance != null) ? info.elapsedDistance : 0.0;
	    	var mDistanceIncrement 		= mElapsedDistance - mPrevElapsedDistance;
	    	var mLapElapsedDistance 	= mElapsedDistance - mLastLapDistMarker;
	    	var mLastNElapsedDistance 	= mElapsedDistance;
	    	if (mTicker > 30) {
	    		mLastNElapsedDistance = mElapsedDistance - mLastNDistanceMarker;
				mLastNDistanceMarker += mDistanceIncrement;
	    	}
	    	var mLapTimerTime = (info.timerTime != null) ? info.timerTime - mLastLapTimeMarker : 0.0;
	    	var mCurrentHeartRate		= (info.currentHeartRate != null) ? info.currentHeartRate : 0;
	    	var mAverageHeartRate		= (info.averageHeartRate != null) ? info.averageHeartRate : 0;
	    	mLapHeartRateAccumulator   += mCurrentHeartRate;
	    	var mLapHeartRate 			= (mLapHeartRateAccumulator / mLapTicker).toNumber();	
	    	
            var idx = curPosRE % mRecentHR.size();
            curPosRE++;
            mRecentHR[idx] = mCurrentHeartRate;

			//! Running economy: http://fellrnr.com/wiki/Running_Economy
			//! Averaged over the last 30 seconds, exponential smoothing applied on top,
			//! decay factor alpha set at 2/(N+1); N=8, alpha and 1-alpha have been pre-computed
			var mLastNAvgHeartRate = 0.0;
			if (mTicker < 30) {
				mLastNAvgHeartRate = getNAvg(mRecentHR, idx+1, mTicker);
			} else {
				mLastNAvgHeartRate = getAverage(mRecentHR);
			}
			var mLastNEconomy = 0.0;
			if (mLastNElapsedDistance > 0) {
				var t = (mTicker < 30) ? mTicker / 60.0 : 0.5;
				mLastNEconomy = ( 1 / ( ((mLastNAvgHeartRate - uRestingHeartRate) * t) / (mLastNElapsedDistance / 1000) ) ) * 100000;   ///Note: Running Economy cm/beat
				//mLastNEconomy =  (mLastNElapsedDistance )/ ((mLastNAvgHeartRate - uRestingHeartRate) * t); ///Note : modify Running Economy to meter per beat heart
			}
			mLastNEconomySmooth = (0.222222 * mLastNEconomy) + (0.777777 * mLastNEconomySmooth);

	        mEconomyField.setData(mLastNEconomySmooth.toNumber());

	        if (mAverageHeartRate > uRestingHeartRate
	        	&& mElapsedDistance > 0) {
	        	mAverageEconomy = ( 1 / ( ( (mAverageHeartRate - uRestingHeartRate) * (info.timerTime / 60000.0) ) / (mElapsedDistance / 1000) ) ) * 100000;   ///Note: Running Economy cm/beat
				//mAverageEconomy = ( (mElapsedDistance) / ((mAverageHeartRate-uRestingHeartRate) * (info.timerTime / 60000.0)) ); ///Note : modify Running Economy to meter per beat heart
	        }
	        mAverageEconomyField.setData(mAverageEconomy.toNumber());

	        if (mLapHeartRate > uRestingHeartRate
	        	&& mLapElapsedDistance > 0) {
	        	mLapEconomy = ( 1 / ( ( (mLapHeartRate - uRestingHeartRate) * (mLapTimerTime / 60000.0) ) / ( mLapElapsedDistance / 1000) ) ) * 100000;         ///Note: Running Economy cm/beat
				mLapEconomy = ( (mLapElapsedDistance) / ( (mLapHeartRate - uRestingHeartRate) * (mLapTimerTime / 60000.0)) ); ///Note : modify Running Economy to meter per beat heart
	        }
	        mLapEconomyField.setData(mLapEconomy.toNumber());
	        
	        mPrevElapsedDistance = mElapsedDistance;
    	}	
		runningEconomy =  mLastNEconomySmooth;
        return mLastNEconomySmooth.toNumber();
		
	  /////////End of Adding Running Economy Compute info////////////////////////


	


    }


    function onLayout(dc) {
        setMetricOrImperial();
        onUpdate(dc);
    }

    function onUpdate(dc) {
       if(HRLayout==false){
			setColors();
        	// reset background
        	var color = getBackgroundColor();
        	dc.setColor(color, color);
        	dc.fillRectangle(0, 0, SCREEN_SIZE, SCREEN_SIZE);

	        drawValues(dc);
		}
		else{
			drawHRLayout(dc);
		} 
				
		
    }

	// updating in v5 to show main fields at top and bottom, and smaller fields in a cluster in the middle
    function drawValues(dc) {
       	// during layout recheck that we need a MAF colour for the screen background
        hasMaf = false;
   
		//drawField(dc, x, y, width, font, fieldSelection, leftAligned)
        drawField(dc, SMALL_INSET, SMALL_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getTopMiddleLeftField(), true);
        drawField(dc, (SCREEN_SIZE/2), SMALL_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getTopMiddleRightField(), false);
        
        drawField(dc, BIG_INSET, BIG_VERTICAL_DOWNSET, SCREEN_SIZE-BIG_INSET, LARGE_FONT, getTopField(), true);
        drawField(dc, BIG_INSET, BIG_SECOND_VERTICAL_DOWNSET, SCREEN_SIZE-BIG_INSET, LARGE_FONT, getBottomField(), true);
        //draw dynamicPace value
        drawField(dc, 18, 110, SCREEN_SIZE-BIG_INSET, Graphics.FONT_SYSTEM_NUMBER_MEDIUM, getSubBottomField(), false);
		//Adding Sub Top Field
		drawField(dc, 18, 80, SCREEN_SIZE-BIG_INSET, Graphics.FONT_NUMBER_MILD, getSubTopField(), false);

        drawField(dc, SMALL_INSET, SMALL_SECOND_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getBottomMiddleLeftField(), true);
        drawField(dc, (SCREEN_SIZE/2), SMALL_SECOND_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getBottomMiddleRightField(), false);

		//draw manual line for 255s
		dc.drawLine((SCREEN_SIZE/2), 25, (SCREEN_SIZE/2), 45);
		dc.drawLine((SCREEN_SIZE/2), 165, (SCREEN_SIZE/2), 185);

        showTrainingEffect(dc);
        showSufferScore(dc);


        if (sufferScore <= 0) {
        		scrollText(dc, "Use Garmin Express to choose which datafields are shown below...");
        }
		

        //dc.drawLine((SCREEN_SIZE/2), 0, (SCREEN_SIZE/2), SCREEN_SIZE);

    }
	
	/////////////////Adding HRLayout Draw Funcionn//////////////////////////
	function drawHRLayout(dc){
			colorHR(dc,120,80, Graphics.FONT_NUMBER_MILD,hrColor);
			colorPacer(dc,20,180, Graphics.FONT_NUMBER_MILD,vDiffColor);
			
			
			dc.setColor(foregroundColour, Graphics.COLOR_TRANSPARENT);

			textC(dc, 170, 85, Graphics.FONT_XTINY,  heartRateLabel);
			textC(dc, 167, 115, Graphics.FONT_NUMBER_HOT, heartRate);
			textC(dc, 155, 160, Graphics.FONT_NUMBER_MEDIUM, minutesToTime(dynamicPace));
			textC(dc, 80, 29, Graphics.FONT_MEDIUM, minutesToTime(averagePace));

        	textC(dc, 60, 160, Graphics.FONT_NUMBER_MEDIUM, currentCadence);
			textC(dc, 155, 30, Graphics.FONT_SMALL,  vTimeDiff);

			
			
			 var d = showLapMetrics ? distance - lastLapStartDistance : distance;
			if (d < 0) {
				d = 0;
			}
			var presentedDistanceValue = d / referenceDistance;
			d = (presentedDistanceValue < 100) ? presentedDistanceValue.format("%.2f") : presentedDistanceValue.format("%.1f");
			drawLapPace(dc, d);

			



			if (invertMiddleBackground == true) {
				// invert the colours of the middle two fields
				dc.setColor(foregroundColour, foregroundColour);	
				dc.fillRectangle(0, 58, 215, 85);
				dc.setColor(backgroundColour, Graphics.COLOR_TRANSPARENT);	
			}
			
				// DRAW GRAPH

			var ii;
			var scaling;
			
			for (var i = 0; i < arrayHRValue.size(); ++i) {
			
				ii = curPos-1-i;
				if(ii < 0) {
					ii = ii + arrayHRValue.size();
				}
				
				if(arrayHRZone[ii] >=0) {
				
					dc.setColor(arrayColours[arrayHRZone[ii]], Graphics.COLOR_TRANSPARENT);
					
					scaling = (arrayHRValue[ii] - HRmin).toFloat() / (HRmax - HRmin).toFloat();
					if(scaling > 1) {
						scaling = 1;
					} else if(scaling < 0) {
						scaling = 0;
					}
					
					dc.drawLine(120-i, 140, 120-i, (140-60*scaling).toNumber());
					
				}
			
        	}
			// Average HR + AVG Pace////////
					textAvgHR(dc, 10, 90, Graphics.FONT_LARGE, avgHR);
					textAvgHR(dc, 80, 90, Graphics.FONT_LARGE, formatAsInt(runningEconomy));        //Note : Running Economy unit heart beat per km
					//textAvgHR(dc, 80, 90, Graphics.FONT_LARGE, (runningEconomy.format("%.1f")));          ///Note : Running Economy mod meter per heart beat
					textAvgHR(dc, 10, 130, Graphics.FONT_LARGE, dynamicStepLenght.format("%.2f"));
					textC(dc, 110, 195, Graphics.FONT_NUMBER_MEDIUM,minutesToTime(currentPace));
					textC(dc, 110, 10, Graphics.FONT_SMALL,sysElapsedDistance);
					textC(dc, 165, 60, Graphics.FONT_LARGE,sysElapsedTime);

					// DRAW LINES

					if (invertMiddleBackground == true) {
						dc.setColor(Graphics.COLOR_DK_GREEN, Graphics.COLOR_TRANSPARENT);
					} else {
						dc.setColor(lineColour, Graphics.COLOR_TRANSPARENT);
					}
					
					dc.drawLine(0, 78, 215, 78);
					dc.drawLine(0, 143, 215, 143);
					dc.drawLine(120,78, 120,143);

	}
	
	////////////////Ending HRLayout Draw Function/////////////////////////////


    function drawField(dc, x, y, width, font, fieldSelection, leftAligned) {   
    
        var fillColor = getColor(fieldSelection);          
    	var inkColor = getInkForBackground(fillColor);
    	
    	var textHeight = 0;
        var fillY = y;
    	
        if (LARGE_FONT == font) {
          	fillY += BIG_BOUNDING_BOX_RELATIVE_HEIGHT;
          	textHeight = BIG_BOUNDING_BOX_RELATIVE_DROP;
		}
        else if (Graphics.FONT_SYSTEM_NUMBER_MEDIUM == font) {
            fillY += BIG_BOUNDING_BOX_RELATIVE_HEIGHT;
          	textHeight = BIG_BOUNDING_BOX_RELATIVE_DROP;
        }
		else {
			fillY += SMALL_BOUNDING_BOX_RELATIVE_HEIGHT;
          	textHeight = SMALL_BOUNDING_BOX_RELATIVE_DROP;
          	// draw a line between the fields
			dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
        	//dc.drawLine(SCREEN_SIZE / 2, fillY, SCREEN_SIZE / 2, fillY + textHeight);///////////////hide line for formula for all watch//////////////
		}
		
		if (null != fillColor && Graphics.COLOR_TRANSPARENT != fillColor) {
            dc.setColor(fillColor, Graphics.COLOR_TRANSPARENT);
            dc.fillRectangle(x, fillY, width, textHeight);
        }
        


        var text = getValue(fieldSelection);
        var alignment = leftAligned ? Graphics.TEXT_JUSTIFY_LEFT : Graphics.TEXT_JUSTIFY_RIGHT;
        // checks if the text can fit, if not then removes the label, then reduces the spacing between characters, 
        // and if still too long deletes characters from the end
    	var predictedWidth = dc.getTextWidthInPixels(text, font);
    	var actualWidth = predictedWidth;
    	
    	// if everything's going to fit then leave space for the label
        if (predictedWidth <= width - LABEL_FONT_SPACING) {       	
        	var textX = leftAligned ? x + LABEL_FONT_SPACING : x + width - LABEL_FONT_SPACING;
        	dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(textX, y, font, text, alignment);
        }
        else {
        	// otherwise use the label space and see if that will fit
        	if (predictedWidth <= width) {
        		//System.println("2-" + text);        		
        		dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
	        	dc.drawText(x, y, font, text, Graphics.TEXT_JUSTIFY_LEFT);

	        }
	        //if it still won't fit then condense the text
	        else {
        		//System.println("3-" + text);
        		if (font != LARGE_FONT) { 
		        	// work out how wide the condensed version is an draw it nearer the middle of the 
		        	// screen if possible
			        actualWidth = condense(dc, x, y, width, font, text, true);
			        if (actualWidth < width) {
			        	if (x < SCREEN_SIZE / 2) {
			        		x = (SCREEN_SIZE / 2) - actualWidth - 4;
		        		}
		        		else {
		        			x = (SCREEN_SIZE / 2) + 4;
		        		}
			        }		        
		        }
		        dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
		        condense(dc, x, y, width, font, text, false);
		        x += (leftAligned? -3: 3);
	        }
        }
        // draw the label in the best place left
        drawLabel(dc, x, fillY, max(actualWidth, width), font, fieldSelection, leftAligned);

        //return textWidth;
        //return width;
    }
    
    function condense(dc, x, y, width, font, text, justCalculateWidth) {
    	var cursor = 0;
        if (null != text && "" != text) {
        	var full = false;
            for (var i = 0; i < text.length(); i++) {
                var letter = text.substring(i, i+1);
                var adjustment = -4;
                var drawX = x + cursor;
                if (":".equals(letter) || ".".equals(letter) || ",".equals(letter)) {
                    adjustment = -1;
                    drawX += 1;
                }
                else {
                	if ("1".equals(letter)) {
                		adjustment = -6;
                	}
                }
                var letterWidth = dc.getTextWidthInPixels(letter, font) + adjustment;
                if (font == LARGE_FONT || cursor + letterWidth <= width) {
                	cursor += letterWidth;
                	if (!justCalculateWidth) {
                		dc.drawText(drawX, y, font, letter, Graphics.TEXT_JUSTIFY_LEFT);
                	}
            	}
            	else {
            		full = true;
            	}
            }
        }
        return cursor;
    }
    
    function drawLabel(dc, x, y, width, font, fieldSelection, leftAligned) {
    	if(!leftAligned) {
    		x += width;
		}
		var isSmallFont = (LARGE_FONT != font);
		if (isSmallFont) {
			y += SMALL_LABEL_FONT_DOWNSET;
		}
		else {
			y += BIG_LABEL_DOWNSET;
		}
        var label = getLabel(fieldSelection, isSmallFont);
        dc.setColor(headerColor, Graphics.COLOR_TRANSPARENT);
        drawVerticalText(dc, x, y, LABEL_FONT, label);
        //System.println("label font width: "  + dc.getTextWidthInPixels("w", LABEL_FONT));
    }


    function setSufferScore() {
        var timeInZone = (elapsedTime - lastMeasuredSufferTime) / 3600000.00;
        lastMeasuredSufferTime = elapsedTime;

        if (1 == currentZone) {
            sufferScore += 12.0 * timeInZone;
        }
        else if (2 == currentZone) {
            sufferScore += 24.0 * timeInZone;
        }
        else if (3 == currentZone) {
            sufferScore += 45.0 * timeInZone;
        }
        else if (4 == currentZone) {
            sufferScore += 100.0 * timeInZone;
        }
        else if (5 == currentZone || 6 == currentZone) {
            sufferScore += 120.0 * timeInZone;
        }
    }
    
    function setTimeInMaf(newElapsedTime) {
    	if ((mafLowerBound <= currentHR) && (currentHR <= mafHigherBound)) {
    		timeInMaf += newElapsedTime - elapsedTime;
    	}
    }
        
    function setTimeOverMaf(newElapsedTime) {
    	if (currentHR > mafHigherBound) {
    		timeOverMaf += newElapsedTime - elapsedTime;
    	}
    }

    function showTrainingEffect(dc) {
        if (null != trainingEffect && 0 != trainingEffect) {
            var message, fillColor;
            var color = textColor;
            if (2 > trainingEffect) {
                message = "EASY RUN";
                fillColor = Graphics.COLOR_WHITE;
            }
            else if (trainingEffect < 3) {
                message = "MAINTAIN FITNESS";
                fillColor = Graphics.COLOR_GREEN;
            }
            else if (trainingEffect < 4) {
                message = "IMPROVE FITNESS";
                fillColor = Graphics.COLOR_YELLOW;
            }
            else if (trainingEffect < 5) {
                message = "LOTS FITTER";
                fillColor = Graphics.COLOR_BLUE;
            }
            else {
                message = "OVERREACHING";
                fillColor = Graphics.COLOR_RED;
            }

            dc.setColor(fillColor, fillColor);
            dc.fillRectangle(0, 236*SCREEN_SIZE/260, SCREEN_SIZE, 26*SCREEN_SIZE/260);

            dc.setColor(color, Graphics.COLOR_TRANSPARENT);
            dc.drawText(SCREEN_SIZE/2, 235*SCREEN_SIZE/260, LABEL_FONT, message, Graphics.TEXT_JUSTIFY_CENTER);
        }
    }

    //Suffer Score Values
    //Activities with a Suffer Score of 100 to 150 will be classified as "tough;" Green
    //those with a Suffer Score of 151 to 250 are "Extreme;" Yellow
    //and those with a suffer Suffer Score above 250 are "Epic." Red
    function showSufferScore(dc) {
//        sufferScore = 251;

        var color = Graphics.COLOR_WHITE;
        if (100 <= sufferScore) {
            if (sufferScore <= 150) {
                color = Graphics.COLOR_GREEN;
            }
            else if (sufferScore <= 250) {
                color = Graphics.COLOR_YELLOW;
            }
            else {
                color = Graphics.COLOR_RED;
            }
        }

        dc.setColor(color, Graphics.COLOR_TRANSPARENT);
        dc.fillRectangle(0, 0, SCREEN_SIZE, SUFFER_BOX_HEIGHT);

        dc.setColor(textColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(SCREEN_SIZE/2, SUFFER_TEXT_DOWNSET, LABEL_FONT, "Suffer: " + formatAsInt(sufferScore), Graphics.TEXT_JUSTIFY_CENTER);
    }

    function getHR() {
        if (currentHR == null) {
            currentHR = 0;
        }
        return currentHR;
    }

    function getAverageHR() {
        if (averageHR == null) {
            averageHR = 0;
        }
        return averageHR;
    }

    function getCurrentCadence() {
        if(currentCadence == null) {
            currentCadence = 0;
        }
        return currentCadence;
    }
	
    function getHRZone(hr) {
        var result;

        if (null == hr || hr < hrZone0) {
            result = 0;
        }
        else if (hr < hrZone1) { // 126
            result = 1;
        }
        else if (hr < hrZone2) { // 138
            result = 2;
        }
        else if (hr < hrZone3) { // 150
            result = 3;
        }
        else if (hr < hrZone4) { // 163
            result = 4;
        }
        else if (hr < hrZone5) { // 175
            result = 5;
        }
        // above zone 5!
        else {
            result = 6;
        }

        return result;
    }

    function getHRColor(hr) {
        var color;
        var zone = null == hr ? currentZone : getHRZone(hr);

        if (null == zone) {
            zone = 0;
        }

        if (2 > zone) { // 126
            color = Graphics.COLOR_WHITE;
        }
        else if (2 == zone) { // 138
            color = Graphics.COLOR_GREEN;
        }
        else if (3 == zone) { // 150
            color = Graphics.COLOR_YELLOW;
        }
        else if (4 == zone) { // 163
            color = Graphics.COLOR_ORANGE;
        }
        else if (5 == zone) { // 175
            color = Graphics.COLOR_RED;
        }
        // above zone 5!
        else {
            color = Graphics.COLOR_PINK;
        }
        return color;
    }

    function getMAFColor() {
        hasMaf = true;
        var color = backgroundColor;

        if (currentHR > mafHigherBound) {
            color = Graphics.COLOR_RED;
        }
        else if (currentHR > mafMiddleBound) {
            color = Graphics.COLOR_GREEN;
        }
        else if (currentHR > mafLowerBound) {
            color = Graphics.COLOR_BLUE;
        }
        else if (currentHR <= mafLowerBound) {
            color = Graphics.COLOR_WHITE;
        }
        return color;
    }

    function getBackgroundColor() {
        var result = backgroundColor;
        if (hasMaf) {
            result = getMAFColor();
        }
        return result;
    }

    function getDistance() {
        var distStr = "0.00";
        if (distance > 0) {
            var distanceKmOrMiles = (distance / kmOrMileInMeters);

            //distanceKmOrMiles += 10;

           if (100 > distanceKmOrMiles) {
                distStr = distanceKmOrMiles.format("%.2f");
            }
            else {
                distStr = distanceKmOrMiles.format("%.1f");
            }
        }
        return distStr;
    }

    function getClock() {
        var clockTime = System.getClockTime();

        var minute = clockTime.min < 10 ? "0" + clockTime.min.format("%.1d") : clockTime.min.format("%.2d");

        return Lang.format("$1$:$2$", [computeHour(clockTime.hour), minute]);

    }


    function getPace() {
        return 100 > currentPace ? minutesToTime(currentPace) : "-:--";
    }

    function getAveragePace() {
        return 100 > averagePace ? minutesToTime(averagePace) : "-:--";
    }
    // Add Dynamic Pace Function
    function getDynamicPace() {
        return 100 > dynamicPace ? minutesToTime(dynamicPace) : "-:--";
    }

    function getCadPace(){
        return 100 > cadPace ? minutesToTime(cadPace) : "-:--";
    }

	/////Adding Running Economy Function to Display///////
	 function getRE() {
        if(runningEconomy== null) {
            runningEconomy = 0;
        }
        return runningEconomy;
    }	

    // Adding Accurate Pace Function to Display/////
    function getAccPace(){
        return 100 > accuratePace? minutesToTime(accuratePace) : "-:--";
    }

    function getDynamicSpeed() {
        if (dynamicSpeed == null) {
           dynamicSpeed = 0;;
        }
        
        return dynamicSpeed.format("%.1f");
    }

    function getDynamicStepLenght() {
        if (dynamicStepLenght == null) {
            dynamicStepLenght = 0;
        }
        return dynamicStepLenght.format("%.2f");
    }

    function getDynamicCadence() {
        if (dynamicCadence == null) {
            dynamicCadence = 0;
        }
        return dynamicCadence;
    }

    function getCadSpeed() {
        if (cadSpeed == null) {
            cadSpeed = 0;            
        }
        return cadSpeed.format("%.1f");
    }
    
    //function getAscent() {
    //   var result = 0.0;
    //    if (1000 != kmOrMileInMeters) {
    //        result = ascent * 3.2808;
    //    }
    //    else {
    //        result = ascent;
    //    }
//        result = 99.4;
    //    return result;
    //}


    function getDuration() {
        var result;
        if (elapsedTime != null && elapsedTime > 0) {
            var minutes = elapsedTime / 1000.0 / 60.0;
            result = minutesToTime(minutes);// + 1000);
        } else {
            result = "0:00";
        }
        return result;
    }
    
    function getPercentageMaf() {
    	var percentageMaf = 0.0;
    	if (timeInMaf != null && timeInMaf > 0) {
    		percentageMaf = (0.0 + timeInMaf) / (0.0 + elapsedTime);
		}
    	// System.println("%MAF = " + percentageMaf);
    	return "" + formatAsInt(percentageMaf * 100) + "%";
    	//return "100%";
    }
    
    function getTimeOverMaf() {
    	var result;
    	if (timeOverMaf != null && timeOverMaf > 0) {
            var minutes = timeOverMaf / 1000.0 / 60.0;
            result = minutesToTime(minutes);
        } else {
            result = "0:00";
        }
        return result;
        //return "10:00";
    }

    function getLabel(fieldSelection, short) {
//        fieldSelection = 3;
		var digits = short ? SHORT_LABEL_NUM_LETTERS : LONG_LABEL_NUM_LETTERS;

        var result = "";
        if (0 == fieldSelection) {
            result = "dist".substring(0, digits);
        }
        else if (1 == fieldSelection) {
            result = "time".substring(0, digits);
        }
        else if (2 == fieldSelection) {
            result = "pace".substring(0, digits);
        }
        else if (3 == fieldSelection) {
            result = "avpc".substring(0, digits);
        }
        else if (4 == fieldSelection) {
            result = "clock".substring(0, digits);
        }
        else if (5 == fieldSelection) {
            result = "hr";
        }
        else if (6 == fieldSelection) {
            result = 2 == digits  ? "ah" : "ahr";
        }
        else if (7 == fieldSelection) {
            result = "ascnt".substring(0, digits);
        }
        else if (8 == fieldSelection) {
            result =  2 == digits ?  "mf" : "maf";
        }
        else if (9 == fieldSelection) {
            result = "%maf".substring(0, digits);
        }
        else if (10 == fieldSelection) {
            result = "omaf".substring(0, digits);
        }
        else if (11==fieldSelection) {
            result = "cad";
        }
        else if (12==fieldSelection) {
            result = "xPace";            
        }
        else if (13==fieldSelection) {
            result ="xSpeed";
        }
        else if (14==fieldSelection) {
            result = "m";
        }
        else if (15==fieldSelection){
            result="xCad";
        }
        else if (16==fieldSelection) {
            result="cSpeed";
        }
       else if (17==fieldSelection){
            result="cPace"; 
       }
       else if (18==fieldSelection){
            result="aPace".substring(0, digits);
       }
	   else if (19==fieldSelection){
            result="RE";
       }
        return result;
       
    }

    function getValue(fieldSelection) {
        var result = "";
        if (0 == fieldSelection) {
            result = getDistance();
            //result = "35.34";
        }
        else if (1 == fieldSelection) {
            result = getDuration();
            //result = "9:29:34";
        }
        else if (2 == fieldSelection) {
            result = getPace();
            //result = "19:59";
        }
        else if (3 == fieldSelection) {
            result = getAveragePace();
        }
        else if (4 == fieldSelection) {
            result = getClock();
            //result ="12:55";
        }
        else if (5 == fieldSelection || 8 == fieldSelection) {
            result = formatAsInt(getHR());
            //result = "999";
        }
        else if (6 == fieldSelection) {
            result = formatAsInt(getAverageHR());
        }
        else if (7 == fieldSelection) {
            //result = formatAsInt(getAscent());
            //result = "10999";
        }
        else if (9 == fieldSelection) {
            result = getPercentageMaf();
            //result = "100%";
        }
        else if (10 == fieldSelection) {
            result = getTimeOverMaf();
            //result = "15:59:23";
        }
        else if(11 == fieldSelection) {
            result = formatAsInt(getCurrentCadence());
            //result ="180";
        }
        else if(12 == fieldSelection) {
            result = getDynamicPace();
            //result = "7:30";
        }
        else if(13 == fieldSelection) {
            result = getDynamicSpeed();
            //result ="2.1";
        }
        else if(14 == fieldSelection) {
            result = getDynamicStepLenght();
            //result ="760"
        }
        else if(15 == fieldSelection){
            result = formatAsInt(getDynamicCadence());
            //result ="180"
        }
        else if (16 == fieldSelection){
            result = getCadSpeed();
            //result ="2.1"
        }
        else if (17 == fieldSelection){
           result = getCadPace();
          //result="7:30"
        }
        else if (18 == fieldSelection){
           result = getAccPace();
          //result="7:30"
        }
		else if (19 == fieldSelection){
           result = formatAsInt(getRE());
          //result="7:30"
        }
        return result;
    }

    function getColor(fieldSelection) {
        var result = Graphics.COLOR_TRANSPARENT;
        if (5 == fieldSelection) {
            result = getHRColor(null);
        }
        else if (6 == fieldSelection) {
            result = getHRColor(getAverageHR());
        }
        else if (8 == fieldSelection) {
            result = getMAFColor();
        }
        return result;
    }

    function getInkForBackground(fillColor) {
        var result = textColor;
        if (0xFF0000 == fillColor) {
            result = Graphics.COLOR_WHITE;
        }
        else if (textColor == fillColor) {
            result = backgroundColor;
        }
        return result;
    }

    function getTopField() {
        var result = getPropertyById("TOP_prop");
        return result;
    }

    function getTopMiddleLeftField() {
        var result = getPropertyById("TML_prop");
        return result;
    }

    function getTopMiddleRightField() {
        var result = getPropertyById("TMR_prop");
//        result = 7;
        return result;
    }

    function getBottomField() {
        return getPropertyById("BOTTOM_prop");
    }

    function getBottomMiddleLeftField() {
        return getPropertyById("BML_prop");
    }


    function getBottomMiddleRightField() {
        return getPropertyById("BMR_prop");
    }
    
    // Adding SUBB_Property
    function getSubBottomField() {
        return getPropertyById("SUB_BOTT_prop");
    }
	/////Adding SUB_TOP_Property
	function getSubTopField() {
        return getPropertyById("SUB_TOP_prop");
    }

    function getPaceFromSpeed(speed) {
        return 0 == speed ? 0 : kmOrMileInMeters/(60 * speed);
    }
    //Add new conver Pace From Speed
    function getPaceFromXSpeed(speed) {
        return 0 == speed ? 0 : 1000/speed;
    }

    function computeHour(hour) {
        if (hour < 1) {
            return hour + 12;
        }
        if (hour >  12) {
            return hour - 12;
        }
        return hour;
    }

    function minutesToTime(minutes) {
        var wholeHours = (minutes/60).toNumber();
        var wholeMinutes = (minutes - (wholeHours * 60)).toNumber();
        var seconds = (60 * (minutes - wholeMinutes - (wholeHours * 60))).toNumber();

        var result = "";
        if (wholeHours > 0) {
            result += wholeHours + ":" + wholeMinutes.format("%02d");
        }
        else {
            result += wholeMinutes;
        }
        result += ":" + seconds.format("%02d");
        return result;

    }

    function drawVerticalText(dc, x, y, font, text) {
        var height = dc.getFontAscent(font)*0.9;
        for (var i = 0; i < text.length(); i++) {
            var letter = text.substring(i, i+1);
            dc.drawText( x, y + (i * height), font, letter, Graphics.TEXT_JUSTIFY_CENTER);
        }
    }


    function formatAsInt(data) {
        var result = "0";
        if (null != data) {
            result = data.format("%.0f");
        }
        return result;
    }

    function resultOrZero(field) {
        return null != field ? field : 0;
    }

    function setColors() {
        var colorSetting = getBackgroundColor();
        if (null != colorSetting && colorSetting != backgroundColor) {
            backgroundColor = colorSetting;
            textColor = (backgroundColor == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
            headerColor = (backgroundColor == Graphics.COLOR_BLACK) ? Graphics.COLOR_LT_GRAY: Graphics.COLOR_DK_GRAY;
        }
    }

    function setMetricOrImperial() {
        if (System.getDeviceSettings().distanceUnits == System.UNIT_METRIC) {
            kmOrMileInMeters = 1000;
        } else {
            kmOrMileInMeters = 1610;
        }
    }

    function getPropertyById(id) {
        var result = App.getApp().getProperty(id);
        if (null == result) {
            result = 0;
        }
        return result;
    }

    function max(left, right) {
        return left > right ? left : right;
    }

    function min(left, right) {
        return left > right ? right : left;
    }


    function scrollText(dc, text) {
    	var x = SCREEN_SIZE - scrollProgress;
    	var textWidth = dc.getTextWidthInPixels(text, LABEL_FONT) + 10;
    	dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_YELLOW);
        dc.fillRectangle(x-2, 0, textWidth, SUFFER_BOX_HEIGHT);

        dc.setColor(textColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(x, SUFFER_TEXT_DOWNSET, LABEL_FONT, text, Graphics.TEXT_JUSTIFY_LEFT);

        scrollProgress += 20;
        if (scrollProgress >= SCREEN_SIZE + textWidth) {
        	scrollProgress = 0;
        }
    }
    /////////////Adding Accurte Pace Funcion//////////////////////////////////////
    function updateLocation() {
		// it's not possible to subscribe to position updates from a datafield, so we're going
		// to have to make do with the data we'll get here; this means we could miss out on a
		// few coordinates if there have been GPS recordings in between 2 compute() calls, but
		// we'll manage... Ideally, these few lines below would just be this:
		// Position.enableLocationEvents(Position.LOCATION_CONTINUOUS, method(:onPositionUpdate));
		// (I'm using Position.getInfo() instead of info.currentLocation because we need the exact
		// timestamp - position.when - of the GPS fix)
		position = Position.getInfo();

		if (
			position.accuracy != Position.QUALITY_NOT_AVAILABLE &&
			position.accuracy != Position.QUALITY_LAST_KNOWN &&
			position.when.compare(self.lastMoment) > 0 &&
			!position.position.toGeoString(Position.GEO_MGRS).equals(self.lastLocation.toGeoString(Position.GEO_MGRS)) &&
			(position.position.toRadians()[0].toFloat() != 0.0 || position.position.toRadians()[1].toFloat() != 0.0) &&
			(position.position.toRadians()[0].toFloat() != Math.PI || position.position.toRadians()[1].toFloat() != Math.PI)
		) {
			self.onPositionUpdate(position);
			self.lastMoment = position.when;
			self.lastLocation = position.position;
		}
	}

    function onPositionUpdate(info) {
		// omit old values - they're no longer relevant
		var now = Time.now();
		var sliceIndex = 0;
		for (var i = 0; i < self.recentMoments.size(); i++) {
			if (now.compare(self.recentMoments[i]) <= self.relevancyWindow) {
				break;
			}
			sliceIndex = i;
		}
		self.recentMoments = self.recentMoments.slice(sliceIndex, null);
		self.recentLocations = self.recentLocations.slice(sliceIndex, null);
		self.recentProjections = self.recentProjections.slice(sliceIndex, null);

		// project the next location based on the new GPS coordinates and the past pace/angle
		var projectedLocation = self.getProjectedLocation(info);

		// store new position & timestamp
		self.recentMoments.add(info.when);
		self.recentLocations.add(info.position);
		self.recentProjections.add(projectedLocation);

		// if distance from GPS becomes too big, then go back and adjust recent projections
		var distanceFromGps = self.calculateDistance(info.position, projectedLocation);
		var accuracyInMeters = self.getLocationAccuracyInMeters(info.accuracy);
		if (distanceFromGps > 0 && distanceFromGps > accuracyInMeters / 2) {
			var fixedPosition = self.getIntermediateCoordinate(
				projectedLocation,
				info.position,
				accuracyInMeters / 2 / distanceFromGps
			);

			// after having "fixed" our most recent projection, which was way off,
			// we'll want to adjust most recent projections as well: we didn't suddenly
			// warp to the new location, but we got there gradually, and apparently
			// we've been projecting that incorrectly
			// we'll simply adjust the existing locations relative the new fixed location
			self.recentProjections[self.recentProjections.size() - 1] = fixedPosition;
			for (var i = self.recentProjections.size() - 2; i > 0; i--) {
				var distanceFromProjection = self.calculateDistance(self.recentProjections[i], projectedLocation);
				var distanceFromFix = self.calculateDistance(self.recentProjections[i], self.recentProjections[self.recentProjections.size() - 1]);
				self.recentProjections[i] = self.getIntermediateCoordinate(
					self.recentProjections[i - 1],
					self.recentProjections[i],
					distanceFromProjection / distanceFromFix
				);
			}
		}
	}

    function getProjectedLocation(info) {
		// so, why is GPS-based pace often erratic?
		// with GPS locations taken at small enough intervals, even the tiniest deviation
		// matters - when taken only a few meters apart, even an armswing is significant
		// paces can be smoothened by averaging them over a larger period of time, but then
		// they'll lag behind, drastically, which kind of defeats the idea behind current pace
		// let's try something different for a change: let's "predict" where our next
		// coordinate will be based on our existing pace & angle, and use the GPS
		// location & accuracy to course correct (e.g. actual change in pace or direction)

		// let's make sure we have sufficient source data before we start deriving
		// more data from it - let's trust GPS until we have a decent enough basis
		if (self.recentProjections.size() < self.reliabilityMinimum || self.recentProjections.size() <= 0) {
			return info.position;
		}

		// projectedLocation is where we predict we'll be based on GPS or data from other
		// sensors (depending on GPS accuracy), smoothened by recent pace & angle
		var lastLocation = self.recentProjections[self.recentProjections.size() - 1];
		return lastLocation.getProjectedLocation(
			self.getProjectedAngle(info),
			self.getProjectedDistance(info)
		);
	}

	function getProjectedDistance(info) {
		var lastLocation = self.recentProjections[self.recentProjections.size() - 1];
		var lastMoment = self.recentMoments[self.recentMoments.size() - 1];
		var duration = info.when.compare(lastMoment);

		// get distance from new GPS location
		var gpsDistance = self.calculateDistance(lastLocation, info.position);

		// derive angle & distance based on recent data
		var recentDistance = 0;
		try {
			var recentPace = self.getRecentPace();
			recentDistance = recentPace > 0 ? duration / recentPace : 0;
		} catch (e instanceof Lang.Exception) {
			return gpsDistance;
		}

		// let's compare recent data against current sensor data (possibly from other
		// sources, like foot pod or accelerometer) to validate it: the closer they match,
		// the better the odds it's accurate
		var sensorsDistance = duration * info.speed;
		var distanceDiffFromSensors = recentDistance > sensorsDistance ? recentDistance - sensorsDistance : sensorsDistance - recentDistance;

		// GPS can be very erratic, but the GPS location and the accuracy of the GPS does
		// give us an indication of where we should expect to be - it's more of an area
		// than it is a specific - detailed - coordinate, though...
		var accuracyInMeters = self.getLocationAccuracyInMeters(info.accuracy);
		var distanceDiffFromGps = recentDistance > gpsDistance ? recentDistance - gpsDistance : gpsDistance - recentDistance;

		// if our data roughly matches the sensors (= not indicating a massive slowdown
		// or pace increase), and we're close enough to the GPS, then just keep using the
		// recent pace without correction
		if (distanceDiffFromSensors < recentDistance / 6 && distanceDiffFromGps < accuracyInMeters / 2) {
			if (recentDistance == sensorsDistance) {
				return recentDistance;
			}
			// use recent data as basis, with a correction toward sensor data
			var sensorsCorrection = distanceDiffFromSensors / sensorsDistance * 6;
			return recentDistance * (1 - sensorsCorrection) + sensorsDistance * sensorsCorrection;
		}

		// if we're not too far from GPS, but not quite dead on, then the sensors data
		// are quite a good indicator to balance out the recent pace
		if (distanceDiffFromGps < accuracyInMeters) {
			if (accuracyInMeters == 0 || sensorsDistance == gpsDistance) {
				return gpsDistance;
			}
			// use sensor data as basis, with a minor correction toward GPS data
			var distanceDiff = sensorsDistance > gpsDistance ? sensorsDistance - gpsDistance : gpsDistance - sensorsDistance;
			var gpsCorrection = (distanceDiff <= sensorsDistance ? distanceDiff / sensorsDistance : distanceDiff / gpsDistance) / accuracyInMeters / 2;
			return sensorsDistance * (1 - gpsCorrection) + gpsDistance * gpsCorrection;
		}

		// there's no salvaging this, all of the data conflicts
		// let's go with recent pace, and then trust that the additional checks
		// later on will leverage GPS in a good enough way to rewrite the history
		// of recent locations
		return recentDistance;
	}

	function getProjectedAngle(info) {
		var lastLocation = self.recentProjections[self.recentProjections.size() - 1];

		// get angle & distance from new GPS locations
		var gpsAngle = self.calculateBearing(lastLocation, info.position);
		var gpsDistance = self.calculateDistance(lastLocation, info.position);

		var recentAngle = 0;
		try {
			recentAngle = self.getRecentAngle();
		} catch (e instanceof Lang.Exception) {
			return gpsAngle;
		}

		// the further away the next GPS coordinate is, and the smaller the accuracy
		// radius, the more precise the angle provided by GPS is (if it's 100m ahead
		// of us, with accuracy up to a few meters, the angle between the outer edges
		// of the accuracy zone is pretty damn small)
		var accuracyInMeters = self.getLocationAccuracyInMeters(info.accuracy);
		var gpsAccuracyAngleDegrees = gpsDistance > 0 && gpsDistance > accuracyInMeters / 2 ? Math.acos((2 * Math.pow(gpsDistance, 2) - Math.pow(accuracyInMeters, 2)) / (2 * Math.pow(gpsDistance, 2))) / (Math.PI / 180.0) : 360;
		var gpsAccuracy = 1 - (gpsAccuracyAngleDegrees / 2 / 180);
		gpsAccuracy = gpsAccuracy > 0 ? gpsAccuracy : 0.001;

		// if the angle we've gotten from GPS has sufficient precision and the angle
		// we've gotten based on recent data is not within the GPS accuracy bounds,
		// it can't be trusted
		var recentAngleDiffFromGps = self.calculateAngle(gpsAngle, recentAngle) / (Math.PI / 180.0);
		if (gpsAccuracyAngleDegrees < 360 && recentAngleDiffFromGps.abs() > gpsAccuracyAngleDegrees / 2) {
			return gpsAngle;
		}

		// figure out how accurate our recent angle is relative to sensor data (e.g. compass)
		// if it's within a 45 degrees angle (22.5 on either side, 1/8 of a full circle)
		// then we're golden
		var sensorsAngle = info.heading;
		var angleDiffDegrees = self.calculateAngle(sensorsAngle, recentAngle) / (Math.PI / 180.0);
		var recentAccuracy = angleDiffDegrees.abs() / 22.5;
		recentAccuracy = recentAccuracy < 1 ? Math.sqrt(1 - recentAccuracy) : 0.001;

		// let's factor in the differences in distance between the new GPS coordinate and
		// our recent pace: if they're far apart, we might want to boost the GPS angle over
		// the recent angle
		// the GPS angle might be way off, but we also may have started turning, and then
		// we'll have to make up even more after having gone in a different direction
		var duration = info.when.compare(lastMoment);
		var recentDistance = 0;
		try {
			var recentPace = self.getRecentPace();
			recentDistance = recentPace > 0 ? duration / recentPace : 0;
		} catch (e instanceof Lang.Exception) {
			// meh...
		}
		var distanceDiffFromGps = recentDistance > gpsDistance ? recentDistance - gpsDistance : gpsDistance - recentDistance;
		if (distanceDiffFromGps > recentDistance / 2 || distanceDiffFromGps > gpsDistance / 2) {
			var distanceMultiplier = distanceDiffFromGps < gpsDistance ? distanceDiffFromGps / gpsDistance : distanceDiffFromGps / recentDistance;
			gpsAccuracy *= distanceMultiplier;
			recentAccuracy *= 1.0 - distanceMultiplier;
		}

		// calculate an angle from combined GPS data & recent direction, based on how
		// accurate we've estimated them both to be (and both could have really poor
		// accuracy, in which case we'll end up somewhere in the middle)
		return Math.atan2(
			Math.sin(gpsAngle) * gpsAccuracy + Math.sin(recentAngle) * recentAccuracy,
			Math.cos(gpsAngle) * gpsAccuracy + Math.cos(recentAngle) * recentAccuracy
		);
	}

	function getRecentPace() {
		if (self.recentProjections.size() < 2) {
			throw new Lang.Exception(/* Insufficient data */);
		}

		// GPS is quite imprecise - even with maximum accuracy, we'll have locations
		// a few meters apart for some time, only for it to then jump 10+ meters
		// all of a sudden, over the same time interval, at the same pace...
		// so our recent pace will be a rolling average over multiple GPS locations,
		// to smoothen out those jumps
		var duration = self.recentMoments[self.recentMoments.size() - 1].compare(self.recentMoments[0]);
		var projectedDistance = 0;
		var gpsDistance = 0;
		for (var i = 0; i < self.recentProjections.size() - 1; i++) {
			projectedDistance += self.calculateDistance(self.recentProjections[i], self.recentProjections[i + 1]);
			gpsDistance += self.calculateDistance(self.recentLocations[i], self.recentLocations[i + 1]);
		}

		// the goal is for the projected distance to be (slightly) less than the GPS
		// distance, which may have some left/right jitter
		// if, however, that is not the case, it's likely because we've strayed from
		// the track (e.g. turned) and using the projected distance would give us an
		// exaggerated pace (more distance covered than is actually the case)
		var distance = gpsDistance < projectedDistance ? gpsDistance : projectedDistance;

		return distance > 0 ? duration / distance : 0;
	}

	function getRecentAngle() {
		if (self.recentProjections.size() < 2) {
			throw new Lang.Exception(/* Insufficient data */);
		}

		// we'll iterate recent positions to determine the current bearing;
		// we'll take the oldest bearing first and keep refining until the most
		// recent bearing
		var recentBearing = self.calculateBearing(self.recentProjections[0], self.recentProjections[1]);
		for (var i = 1; i < self.recentProjections.size() - 1; i++) {
			var bearing = self.calculateBearing(self.recentProjections[i], self.recentProjections[i + 1]);
			var diffInDegrees = self.calculateAngle(recentBearing, bearing) / (Math.PI / 180.0);
			diffInDegrees = diffInDegrees > 0 ? diffInDegrees : -diffInDegrees;
			if (diffInDegrees > 11.25) {
				// if the angle is not within a 22.5 degrees (11.25 on either side) of the
				// most recent angle, we've probably turned and the older bearings become
				// irrelevant
				recentBearing = bearing;
			}

			// refine the current bearing using this bearing;
			// most recent bearings will be calculated last, so they'll have the most impact
			recentBearing = Math.atan2(
				Math.sin(recentBearing) + Math.sin(bearing),
				Math.cos(recentBearing) + Math.cos(bearing)
			);
		}

		return recentBearing;
	}

	function haversin(theta) {
		return (1 - Math.cos(theta)) / 2;
	}

	function calculateDistance(location1, location2) {
		var lat1 = location1.toRadians()[0];
		var lng1 = location1.toRadians()[1];
		var lat2 = location2.toRadians()[0];
		var lng2 = location2.toRadians()[1];

		if (lat1 == lat2 && lng1 == lng2) {
			return 0;
		}

		var h = self.haversin(lat2 - lat1) + Math.cos(lat1) * Math.cos(lat2) * self.haversin(lng2 - lng1);
		return 2 * self.earthMeanRadius * Math.asin(Math.sqrt(h));
	}

	function calculateBearing(location1, location2) {
		var lat1 = location1.toRadians()[0];
		var lng1 = location1.toRadians()[1];
		var lat2 = location2.toRadians()[0];
		var lng2 = location2.toRadians()[1];

		var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1);
		var y = Math.cos(lat2) * Math.sin(lng2 - lng1);

		var bearing = Math.atan2(y, x);

		// normalize to negative values west & positive values east
		return bearing > Math.PI ? bearing - 2.0 * Math.PI : bearing;
	}

	function calculateAngle(bearing1, bearing2) {
		var bearing1y = Math.cos(bearing1);
		var bearing1x = Math.sin(bearing1);
		var bearing2y = Math.cos(bearing2);
		var bearing2x = Math.sin(bearing2);

		var sign = bearing1y * bearing2x >= bearing2y * bearing1x ? 1.0 : -1.0;
		return sign * Math.acos(bearing1x * bearing2x + bearing1y * bearing2y);
	}

	function getIntermediateCoordinate(location1, location2, fraction) {
		if (fraction == 0) {
			return location1;
		}

		if (fraction == 1) {
			return location2;
		}

		var angularDistance = self.calculateDistance(location1, location2) / self.earthMeanRadius;
		if (angularDistance == 0) {
			return location1;
		}

		var lat1 = location1.toRadians()[0];
		var lng1 = location1.toRadians()[1];
		var lat2 = location2.toRadians()[0];
		var lng2 = location2.toRadians()[1];

		var a = Math.sin((1 - fraction) * angularDistance) / Math.sin(angularDistance);
		var b = Math.sin(fraction * angularDistance) / Math.sin(angularDistance);
		var x = a * Math.cos(lat1) * Math.cos(lng1) + b * Math.cos(lat2) * Math.cos(lng2);
		var y = a * Math.cos(lat1) * Math.sin(lng1) + b * Math.cos(lat2) * Math.sin(lng2);
		var z = a * Math.sin(lat1) + b * Math.sin(lat2);
		var lat = Math.atan2(z, Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
		var lng = Math.atan2(y, x);

		return new Position.Location({:latitude => lat, :longitude => lng, :format => :radians});
	}

	function getDOP(accuracy) {
		// a guess at the dilution of precision based on Garmin's GPS quality constants
		// @see https://developer.garmin.com/downloads/connect-iq/monkey-c/doc/Toybox/Position.html
		// @see https://en.wikipedia.org/wiki/Dilution_of_precision_(navigation)
		var qualityToDOP = {
			//Position.QUALITY_NOT_AVAILABLE => 9999999999999, // useless
			//Position.QUALITY_LAST_KNOWN => 9999999999999, // useless
			Position.QUALITY_POOR => 14,
			Position.QUALITY_USABLE => 7,
			Position.QUALITY_GOOD => 3,
		};

		return qualityToDOP[accuracy];
	}

	function getLocationAccuracyInMeters(accuracy) {
		// by their own account, Garmin devices should be accurate up to 3m
		// @see https://support.garmin.com/en-US/?faq=P3DdzRfgik3fky125aHsFA
		var bestAccuracy = 3.0;

		// while not reliable, multiplying best accuracy with HDOP will provide
		// a rough guesstimate in meters
		// @see https://gis.stackexchange.com/questions/377/calculating-absolute-precision-confidence-number-from-dilution-of-precision-indi
		return self.getDOP(accuracy) * bestAccuracy;
	}

	function convertUnits(minutesPerKm) {
		if (self.units == System.UNIT_STATUTE) {
			return minutesPerKm / 0.621371192;
		}
		return minutesPerKm;
	}

	function formatTime(minutes) {
		var seconds = Math.round((minutes * 60).toLong() % 60 / 5) * 5;
		return Lang.format("$1$:$2$", [minutes.format("%d"), seconds.format("%02d")]);
	}

    //////////////End Accurate Pace Function/////////////////////////////////

	//////////////Adding Running Economy Function///////////////////////
	 function onTimerLap() {
    	var info = Activity.getActivityInfo();

    	mLastLapTimerTime			= (info.timerTime - mLastLapTimeMarker) / 1000;
    	mLastLapElapsedDistance		= (info.elapsedDistance != null) ? info.elapsedDistance - mLastLapDistMarker : 0;
    	mLastLapEconomy				= mLapEconomy;
		
    	mLaps++;
    	mLapTicker = 0;
    	mLastLapDistMarker 			= info.elapsedDistance;
    	mLastLapTimeMarker 			= info.timerTime;
    	mLapHeartRateAccumulator 	= 0;
    	mLapEconomy					= 0;

		paceChartData.add(lapAvgPace);
        lastLapStartTimer = elapsedTime;
        lastLapStartDistance = distance;
    	
    }

    //! Timer transitions from stopped to running state
    function onTimerStart() {
    	mTimerRunning = true;
    }


    //! Timer transitions from running to stopped state
    function onTimerStop() {
    	mTimerRunning = false;
    }


    //! Timer transitions from paused to running state (i.e. resume from Auto Pause is triggered)
    function onTimerResume() {
    	mTimerRunning = true;
    }


    //! Timer transitions from running to paused state (i.e. Auto Pause is triggered)
    function onTimerPause() {
    	mTimerRunning = false;
    }


    //! Current activity is ended
    function onTimerReset() {
		mPrevElapsedDistance = 0;

		mLaps 					  = 1;
		mLastLapDistMarker 		  = 0;
	    mLastLapTimeMarker 		  = 0;
	    mLapHeartRateAccumulator  = 0;

		mLastLapTimerTime 		= 0;
		mLastLapElapsedDistance = 0;
		mLastLapEconomy			= 0;

		mLastNDistanceMarker = 0;
		mLastNEconomySmooth	 = 0;
		mAverageEconomy		 = 0;
		mLapEconomy			 = 0;
		
		mTicker 	= 0;
		mLapTicker	= 0;
		
		for (var i = 0; i < mRecentHR.size(); ++i) {
            mRecentHR[i] = 0.0;
        }
        curPosRE = 0;		
    }
    
    
    function getAverage(a) {
        var count = 0;
        var sum = 0.0;
        for (var i = 0; i < a.size(); ++i) {
            if (a[i] > 0.0) {
                count++;
                sum += a[i];
            }
        }
        if (count > 0) {
            return sum / count;
        } else {
            return null;
        }
    }


    function getNAvg(a, curIdx, n) {
        var start = curIdx - n;
        if (start < 0) {
            start += a.size();
        }
        var count = 0;
        var sum = 0.0;
        for (var i = start; i < (start + n); ++i) {
            var idx = i % a.size();
            if (a[idx] > 0.0) {
                count++;
                sum += a[idx];
            }
        }
        if (count > 0) {
            return sum / count;
        } else {
            return null;
        }
    }

	////////////End of Running Economy Function////////////////////////

	//////Adding Snaphost HR Function////////////

function toDist(dist) {
		if (dist == null) {
			return "0.00";
		}

		dist = dist / 1000;
		return dist.format("%.2f", dist);
	}

    function toStr(o) {
		if (o != null && o > 0) {
			return "" + o;
		} else {
			return "---";
		}
	}

    function fmtTime(clock) {

		var h = clock.hour;

		if (System.getDeviceSettings().is24Hour) {
			if (h > 12) {
				h -= 12;
			} else if (h == 0) {
				h += 12;
			}
		}

		return "" + h + ":" + clock.min.format("%02d");
	}


	function fmtSecs(secs) {

		if (secs == null) {
			return "---";
		}

		var s = secs.toLong();
		var hours = s / 3600;
		s -= hours * 3600;
		var minutes = s / 60;
		s -= minutes * 60;

		var fmt;
		if (hours > 0) {
			fmt = "" + hours + ":" + minutes.format("%02d") + ":" + s.format("%02d");
		} else {
			fmt = "" + minutes + ":" + s.format("%02d");
		}

		return fmt;
	}

    function textC(dc, x, y, font, s) {
		if (s != null) {
			dc.drawText(x, y, font, s, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		}
	}

    function textAvgHR(dc, x, y, font, s) {
		if (s != null) {
			dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
            dc.drawText(x, y, font, s, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);
		}
	}

    function colorHR(dc, x, y, font, s) {
		if (s != null) {
			//dc.drawText(x, y, font, s, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
			dc.setColor(s,Graphics.COLOR_TRANSPARENT);
		    dc.fillRectangle(121, 79, 120, 65);
		}
	
	}

    function colorPacer(dc, x, y, font, s) {
		if (s != null) {
			//dc.drawText(x, y, font, s, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
			dc.setColor(s,Graphics.COLOR_TRANSPARENT);
		    //dc.fillRectangle(0, 145, 80, 145);
            dc.fillRoundedRectangle(130, 15, 50, 30, 40);
		}
	
	}


	
    

////////End of Adding SNapshot HR Function//////////////


////////////////////////Adding Virtual Pacer Function/////////////////

 /////////Add minute Pace Function///////////////////////
     function minutesToTimePacer(minutes) {
        var wholeHours = (minutes/60).toNumber();
        var wholeMinutes = (minutes - (wholeHours * 60)).toNumber();
        var seconds = (60 * (minutes - wholeMinutes - (wholeHours * 60))).toNumber();

        var result = "";
        if (wholeHours > 0) {
            result += wholeHours + ":" + wholeMinutes.format("%02d");
        }
        else {
            result += wholeMinutes;
        }
        return result;

    }
///////////////////////Eding Virtual Pacer Function//////////////////////

///////////////////////Adding Lap Pace Function////////////////////////
 		function drawLapPace(dc, presentedDistance) {
        
         dc.setColor(textColor, Graphics.COLOR_TRANSPARENT);
        //var displayHr = hr > 0 ? hr.format("%d") : "-";
       // dc.drawText(mainMetricsX, centerY - fourthY, value_font, displayHr, Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(20, 59, Graphics.FONT_NUMBER_MEDIUM, displayPace(lapAvgPace), Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);
        //dc.drawText(105, 160, Graphics.FONT_NUMBER_MEDIUM, getLapAveragePace(), Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);

        // distance unit
        //dc.setColor(darkColor, Graphics.COLOR_TRANSPARENT);
        //dc.drawText(centerX + dc.getTextWidthInPixels(presentedDistance, value_font)>>1 + 5, 40, label_font, distanceString, Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);

        // distance
        //dc.drawText(centerX, fourthY, value_font, presentedDistance, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);

        // duration
        //var d = showLapMetrics ? elapsedTime - lastLapStartTimer : elapsedTime;

        //var seconds = d / 1000;
        //var minutes = seconds / 60;
        //var hours = minutes / 60;
        //seconds %= 60;
        //minutes %= 60;

        //if (hours > 0) {
        //    d = Lang.format("$1$:$2$:$3$", [hours, minutes.format("%02d"), seconds.format("%02i")]);
        //} else {
        //    d = Lang.format("$1$:$2$", [minutes, seconds.format("%02i")]);
        //}
        //dc.drawText(centerX, height-fourthY, value_font, d, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }


	 function displayPace(pace) {
        if (pace == null || pace == 0) {
            return "0:00";
        }
        var seconds = pace.toNumber();
        var minutes = seconds / 60;
        seconds %= 60;
        return Lang.format("$1$:$2$", [minutes, seconds.format("%02d")]);
    }



	
////////////////////////Eding Lap Pace Function/////////////////////////






        
    function setWatchType() {
    	//6pro 006-B3290-00
        //6xpro 006-B3291-00
        //245 006-B3076-00
        //245M 006-B3077-00
        //255 006-B3992-00, 3992
        //255M 006-B3990-00, 3990, 260
        //255S 006-B3993-00, 3993, 218
        //255SM 006-B3991-00, 3991, 218
        //645 006-B2886-00
        //645M 006-B2888-00
        //6 006-B3289-00
        //945 006-B3113-00
        //945LTE 006-B3652-00
        //5XPlus 006-B3111-00
        //6SPro 006-B3288-00
        //6S 006-B3287-00
        //6Chronos 006-B2432-00
        //7 006-B3906-00, 3906, 260
        //7S 006-B3905-00, 3905, 240
        //7X 006-B3907-00, 3907, 280
        //5X 006-B2604-00
        //5SPlus 006-B2900-00
        //5S 006-B2544-00
        //5Plus 006-B3110-00
        //5 006-B2697-00
        //745 006-B3589-00
        //935 006-B2691-00
        //Venu2 006-B3703-00
        //Venu2S 006-B3704-00
        //Venu 006-B3226-00
        //VenuMB 006-B3740-00
        //Vivoactive4 006-B3225-00
        //Vivoactive4S 006-B3224-00
        var partNumber = "";
        var id = System.getDeviceSettings().partNumber;
        if (null != id && 9 < id.length()) {
        	partNumber = id.substring(5,9);
        } 
        /*
        System.println("id: " + id);
        System.println("partNumber: " + partNumber);
        System.println("SCREEN_SIZE: " + SCREEN_SIZE);
        */
            
        if (218 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "2432":
	        		watchType = F6C;
	        		break;
	        	case "2544":
	        		watchType = F5S;
	        		break;
	        	case "3224":
	        		watchType = Vivo4S;
	        		break;
	        	case "3993":
	        		watchType = F255S;
	        		break;
	        	case "3991":
	        		watchType = F255SM;
	        		break;
				default:		
	        }
        }
        
        if (240 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3076":
	        		watchType = F245;
	        		break;
	    		case "3077":
	    			watchType = F245M;
	    			break;
				case "2886":
	    			watchType = F645;
	    			break;
				case "2888":
	    			watchType = F645M;
	    			break;
	    		case "3113":
	    			watchType = F945;
	    			break;	
	    		case "3652":
	    			watchType = F945LTE;
	    			break;
	    		case "3111":
	    			watchType = F5XPlus;
	    			break;	
	    		case "3288":
	    			watchType = F6SPro;
	    			break;	
	    		case "3287":
	    			watchType = F6S;
	    			break;	
	    		case "2604":
	    			watchType = F5X;
	    			break;	
	    		case "2900":
	    			watchType = F5SPlus;
	    			break;	
	    		case "3110":
	    			watchType = F5Plus;
	    			break;
	    		case "2697":
	    			watchType = F5;
	    			break;
	    		case "3589":
	    			watchType = F745;
	    			break;	
	    		case "2691":
	    			watchType = F935;
	    			break;	
	    		case "3905":
	    			watchType = F7S;
	    			break;		
	    			
				default:	
	        }
        } 
              
        if (260 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3289":
	        		watchType = F6;
	        		break;
	    		case "3290":
	    			watchType = F6Pro;
	    			break;
	    		case "3225":
	    			watchType = Vivo4;
	    			break;
	    		case "3992":
	    			watchType = F255;
	    			break;
	    		case "3990":
	    			watchType = F255M;
	    			break;
	    		case "3906":
	    			watchType = F7;
	    			break;
				default:		
	        }
        }
        
        if (280 == SCREEN_SIZE) {
        	switch (partNumber) {
	    		case "3907":
	    			watchType = F7X;
	    			break;
				default:	
					watchType = F6XPro;
	        }
        }
              
        if (360 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3704":
	        		watchType = Venu2S;
	        		break;
				default:		
	        }
        }
              
        if (390 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3226":
	        		watchType = Venu;
	        		break;
				default:		
	        }
        	switch (partNumber) {
	        	case "3740":
	        		watchType = VenuMB;
	        		break;
				default:		
	        }
        }
              
        if (416 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3703":
	        		watchType = Venu2;
	        		break;
				default:		
	        }
        }
    }
    
    function setWatchLayout(watchType) {
    	switch (watchType) {
        	case F245:
        	case F245M:
        	case F945:
        	case F945LTE:
        	case F745:
        		SMALL_VERTICAL_DOWNSET = 16;
	        	BIG_VERTICAL_DOWNSET = 42;
			    BIG_SECOND_VERTICAL_DOWNSET = 99;
			    SMALL_SECOND_VERTICAL_DOWNSET = 167;
	        	BIG_INSET = 15;
			    SMALL_INSET = 33;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 10;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 22;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 37;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 5;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
			    
			    break;
			    
			
        	case F6SPro:
        	case F6S:
        	case F7S:
        		SMALL_VERTICAL_DOWNSET = 9;
	        	BIG_VERTICAL_DOWNSET = 38;
			    BIG_SECOND_VERTICAL_DOWNSET = 95;
			    SMALL_SECOND_VERTICAL_DOWNSET = 158;
	        	BIG_INSET = 15;
			    SMALL_INSET = 28;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 15;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 23;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 39;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 5;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 4;
    			SUFFER_TEXT_DOWNSET = -2;
				SUFFER_BOX_HEIGHT = 19;
			    
			    break;
        	
			    
			case F645:
			case F645M:
			case F5XPlus:
        	case F5X:
        	case F5SPlus:
        	case F5Plus:
        	case F5:
        	case F935:
				SMALL_VERTICAL_DOWNSET = 22;
	        	BIG_VERTICAL_DOWNSET = 60;
			    BIG_SECOND_VERTICAL_DOWNSET = 119;
			    SMALL_SECOND_VERTICAL_DOWNSET = 178;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 40;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 62;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = -2;
				SUFFER_BOX_HEIGHT = 19;
			    
			    break;
			    
			case F6C:
			case F5S:
				SMALL_VERTICAL_DOWNSET = 15;
	        	BIG_VERTICAL_DOWNSET = 51;
			    BIG_SECOND_VERTICAL_DOWNSET = 108;
			    SMALL_SECOND_VERTICAL_DOWNSET = 166;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    MEDIUM_FONT = Graphics.FONT_SYSTEM_LARGE;
				//LABEL_FONT = Graphics.FONT_SYSTEM_TINY;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 5;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 26;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = -4;
				SUFFER_BOX_HEIGHT = 17;
			    
			    break;
			    
			case Vivo4S:
			case F255S:
			case F255SM:
				SMALL_VERTICAL_DOWNSET = 20;
	        	BIG_VERTICAL_DOWNSET = 40;
			    BIG_SECOND_VERTICAL_DOWNSET = 92;
			    SMALL_SECOND_VERTICAL_DOWNSET = 158;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    MEDIUM_FONT = Graphics.FONT_SYSTEM_LARGE;
				//LABEL_FONT = Graphics.FONT_SYSTEM_TINY;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 5;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 26;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = 0;
				SUFFER_BOX_HEIGHT = 17;
				BIG_LABEL_DOWNSET = 22;
			    
			    break;
			    
		    case F6Pro:
		    case F6:
		    case F255:
		    case F255M:
		    case F7:
				SMALL_VERTICAL_DOWNSET = 19;
	        	BIG_VERTICAL_DOWNSET = 43;
			    BIG_SECOND_VERTICAL_DOWNSET = 107;
			    SMALL_SECOND_VERTICAL_DOWNSET = 184;
		        BIG_INSET = 12;
			    SMALL_INSET = 32;
			    MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 13;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 24;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 36;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 65;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;
			    
		    case Vivo4:
				SMALL_VERTICAL_DOWNSET = 25;
	        	BIG_VERTICAL_DOWNSET = 49;
			    BIG_SECOND_VERTICAL_DOWNSET = 109;
			    SMALL_SECOND_VERTICAL_DOWNSET = 180;
		        BIG_INSET = 12;
			    SMALL_INSET = 32;
			    //MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 10;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 23;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 38;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 58;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;		   
			    
		    case F6XPro:
		    case F7X:
		    	SMALL_VERTICAL_DOWNSET = 17;
	        	BIG_VERTICAL_DOWNSET = 43;
			    BIG_SECOND_VERTICAL_DOWNSET = 114;
			    SMALL_SECOND_VERTICAL_DOWNSET = 195;
		        BIG_INSET = 14;
			    SMALL_INSET = 38;
			    MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 14;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 26;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 38;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 68;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;
			    
			case Venu2:
		    	SMALL_VERTICAL_DOWNSET = 28;
	        	BIG_VERTICAL_DOWNSET = 70;
			    BIG_SECOND_VERTICAL_DOWNSET = 165;
			    SMALL_SECOND_VERTICAL_DOWNSET = 275;
		        BIG_INSET = 20;
			    SMALL_INSET = 55;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 24;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 39;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 57;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 96;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = 5;				
				SHORT_LABEL_NUM_LETTERS = 2;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
			case Venu:
			case VenuMB:
		    	SMALL_VERTICAL_DOWNSET = 24;
	        	BIG_VERTICAL_DOWNSET = 72;
			    BIG_SECOND_VERTICAL_DOWNSET = 172;
			    SMALL_SECOND_VERTICAL_DOWNSET = 282;
		        BIG_INSET = 20;
			    SMALL_INSET = 55;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 13;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 22;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 59;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 103;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = 0;				
				SHORT_LABEL_NUM_LETTERS = 2;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
			case Venu2S:
		    	SMALL_VERTICAL_DOWNSET = 24;
	        	BIG_VERTICAL_DOWNSET = 66;
			    BIG_SECOND_VERTICAL_DOWNSET = 151;
			    SMALL_SECOND_VERTICAL_DOWNSET = 252;
		        BIG_INSET = 20;
			    SMALL_INSET = 45;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 18;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 33;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 57;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 87;
				LABEL_FONT_SPACING = 5;
				SMALL_LABEL_FONT_DOWNSET = 0;				
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
		    default:   	
        
        }
    }
        
    function printWatchType() {
    	var display = "";
    	switch (watchType) {
    		case F245:
    			display = "Forerunner 245";
    			break;
    		case F245M:
    			display = "Forerunner 245 Music";
    			break;
    		case F645:
    			display = "Forerunner 645";
    			break;
    		case F645M:
    			display = "Forerunner 645 Music";
    			break;
    		case F6Pro:
    			display = "Fenix 6 Pro";
    			break;
    		case F6XPro:
    			display = "Fenix 6X Pro";
    			break;
    		case F6:
    			display = "Fenix 6";
    			break;
    		case F945:
    			display = "Forerunner 945";
    			break;
    		case F945LTE:
    			display = "Forerunner 945 LTE";
    			break;
    		case F5XPlus:
    			display = "Forerunner 5X Plus";
    			break;
    		case F6SPro:
    			display = "Fenix 6S Pro";
    			break;
    		case F6S:
    			display = "Fenix 6S";
    			break;
    		case F5X:
    			display = "Fenix 5X";
    			break;
    		case F5SPlus:
    			display = "Fenix 5S Plus";
    			break;
    		case F5S:
    			display = "Fenix 5S";
    			break;
    		case F5Plus:
    			display = "Fenix 5 Plus";
    			break;
    		case F5Plus:
    			display = "Fenix 5";
    			break;
    		case F745:
    			display = "Fenix 745";
    			break;
    		case Venu2:
    			display = "Venu 2";
    			break;
    		case Venu2S:
    			display = "Venu 2S";
    			break;
    		case Venu:
    			display = "Venu";
    			break;
    		case VenuMB:
    			display = "Venu Mercedes Benz";
    			break;
    		case Vivo4:
    			display = "Vivoactive 4";
    			break;
    		case Vivo4S:
    			display = "Vivoactive 4S";
    			break;

    		default: 
    			display = "Not listed";
			
    	}
    	
    	System.println(display);
    }


}